#!/usr/bin/make -f

GOSRC := $(shell find . -name '*.go')
GOTESTDATA := $(shell find testdata -type f)

GOCOVERAGE ?= go.coverage

all:

lint:
	go run honnef.co/go/tools/cmd/staticcheck@2023.1.3 ./...
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@v1.53.3 run -E gofmt ./...

fmt:
	go mod tidy
	gofmt -w -s $(GOSRC)

test: GOTESTFLAGS ?= -race
test:
	go test $(GOTESTFLAGS) ./...

mutate: test
	go-mutesting ./...

clean:
	$(RM) $(GOCOVERAGE).html $(GOCOVERAGE).out

$(GOCOVERAGE).out: GOTESTCOVERFLAGS ?= -cover -covermode=atomic -coverprofile=$@
$(GOCOVERAGE).out: $(GOSRC) $(GOTESTDATA) go.mod
	$(MAKE) test GOTESTFLAGS="$(GOTESTFLAGS) $(GOTESTCOVERFLAGS)"

$(GOCOVERAGE).html: $(GOCOVERAGE).out
	go tool cover -html $< -o $@

$(GOCOVERAGE): $(GOCOVERAGE).out
	go tool cover -html $<


.PHONY: all test lint fmt mutate
.DELETE_ON_ERROR:
