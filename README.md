# `percent`

[![Go
Reference](https://pkg.go.dev/badge/codeberg.org/piman/percent.svg)](https://pkg.go.dev/codeberg.org/piman/percent)

`codeberg.org/piman/percent` implements a [transform.Transformer][] for
percent encoding (aka URL encoding, query escaping, etc.) from [RFC
3986][] §2.1; and another for decoding.

This is similar to `net/url.EscapePath`, `net/url.UnescapePath`, and so
on. If you need to handle short strings of regular URL components those
may be preferable. The transformers here help handle unbounded streams
of data, unusual escaping requirements, or percent encodings as one
possible transformation alongside others.


[transform.Transformer]: https://pkg.go.dev/golang.org/x/text/transform#Transformer
[RFC 3986]: https://rfc-editor.org/rfc/rfc3986.html


## License

Copyright 2023 Joe Wreschnig

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either [version 3 of the License](COPYING), or
(at your option) any later version.
