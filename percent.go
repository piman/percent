// Package percent implements a [transform.Transformer] for percent
// encoding (aka URL encoding, query escaping, etc.) from RFC 3986 §2.1;
// and another for decoding.
//
// This is similar to [net/url.EscapePath], [net/url.UnescapePath], and
// so on. If you need to handle short strings of regular URL components
// those may be preferable. The transformers here help handle unbounded
// streams of data, unusual escaping requirements, or percent encodings
// as one possible transformation alongside others.
package percent

import (
	"net/url"

	"golang.org/x/text/transform"
)

// Bytes indicate byte values which can represent themselves when
// encoded. Bytes are immutable; modifications return new Bytes. A zero
// Bytes permits no value to represent itself.
type Bytes struct {
	bits [4]uint64
}

// Allowed checks if a byte value may represent itself.
func (b Bytes) Allowed(x byte) bool {
	return b.bits[x/64]&(1<<(x%64)) != 0
}

// Allow allows the given bytes to represent themselves when encoded.
func (b Bytes) Allow(xs ...byte) Bytes {
	for _, x := range xs {
		b.bits[x/64] |= 1 << (x % 64)
	}
	return b
}

// Reserve reserves the given bytes; they may not represent themselves
// when encoded and will be escaped.
func (b Bytes) Reserve(xs ...byte) Bytes {
	for _, x := range xs {
		b.bits[x/64] &= ^(1 << (x % 64))
	}
	return b
}

// AllowRange allows all bytes between the two values, inclusively.
func (b Bytes) AllowRange(lo, hi byte) Bytes {
	for x := lo; x <= hi; x++ {
		b.bits[x/64] |= 1 << (x % 64)
	}
	return b
}

// NewEncoder return a percent-encoding transformer per RFC 3986 §2.1.
// These encoders are stateless; they does not need to be reset and can
// safely be used concurrently. ‘%’ is always escaped (as ‘%25’)
// regardless of the allowed bytes.
func NewEncoder(allowed Bytes) transform.Transformer {
	return &encoder{bytes: allowed.Reserve('%')}
}

type encoder struct {
	transform.NopResetter
	bytes Bytes
}

func unhex(c byte) byte {
	return ("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\xff\xff\xff\xff\xff\xff" +
		"\xff\x0a\x0b\x0c\x0d\x0e\x0f\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\x0a\x0b\x0c\x0d\x0e\x0f\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff" +
		"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff")[c]
}

func (e *encoder) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
	const tohex = "0123456789ABCDEF"
	for nSrc < len(src) && nDst < len(dst) {
		if b := src[nSrc]; e.bytes.Allowed(b) {
			dst[nDst] = b
			nSrc++
			nDst++
		} else if nDst+2 >= len(dst) {
			break
		} else {
			dst[nDst] = '%'
			dst[nDst+1] = tohex[b>>4]
			dst[nDst+2] = tohex[b&0xF]
			nDst += 3
			nSrc++
		}
	}
	if nSrc < len(src) {
		err = transform.ErrShortDst
	}
	return
}

type strictDecoder struct {
	transform.NopResetter
}

func (strictDecoder) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
	for nSrc < len(src) && nDst < len(dst) {
		if b := src[nSrc]; b != '%' {
			dst[nDst] = b
			nSrc++
			nDst++
		} else if nSrc+2 < len(src) {
			hi, lo := unhex(src[nSrc+1]), unhex(src[nSrc+2])
			if hi == 0xFF || lo == 0xFF {
				return nDst, nSrc, url.EscapeError(src[nSrc : nSrc+3])
			}
			dst[nDst] = hi<<4 | lo
			nSrc += 3
			nDst++
		} else if atEOF {
			return nDst, nSrc, url.EscapeError(src[nSrc:])
		} else {
			return nDst, nSrc, transform.ErrShortSrc
		}
	}
	if nSrc < len(src) {
		err = transform.ErrShortDst
	}
	return
}

type laxDecoder struct {
	transform.NopResetter
}

func (laxDecoder) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
	for nSrc < len(src) && nDst < len(dst) {
		b := src[nSrc]
		if b == '%' && nSrc+2 < len(src) {
			hi, lo := unhex(src[nSrc+1]), unhex(src[nSrc+2])
			if hi != 0xFF && lo != 0xFF {
				dst[nDst] = hi<<4 | lo
				nSrc += 3
				nDst++
				continue
			}
		} else if !atEOF {
			return nDst, nSrc, transform.ErrShortSrc
		}
		dst[nDst] = b
		nSrc++
		nDst++
	}
	if nSrc < len(src) {
		err = transform.ErrShortDst
	}
	return
}

var (
	// URIEncoder percent-escapes all bytes except ASCII
	// alphanumerics and ‘-’, ‘.’, and ‘_’, unreserved in RFC 3986
	// §2.3. ‘~’ is still escaped as it was historically also
	// reserved. (E.g. it is not present in RFC 1738 “safe”.)
	//
	// Percent encoding has been used in many contexts with slightly
	// differing sets of allowed bytes. This encoder should be safe
	// in most of them.
	URIEncoder = NewEncoder(Bytes{}.
			AllowRange('0', '9').
			AllowRange('A', 'Z').
			AllowRange('a', 'z').
			Allow('-', '.', '_'))

	// Decoder percent-decodes text per RFC 3986 §2.2. It is capable
	// of decoding any percent-encoded text regardless of the
	// encoder.
	//
	// Decoded text will never be longer than encoded text, so you
	// can safely use the same buffer for input and output of any
	// transformation.
	//
	// If the decoder encounters malformed input it returns a
	// [url.EscapeError].
	Decoder transform.Transformer = strictDecoder{}

	// LaxDecoder percent-decodes text per the WHATWG [URL Living
	// Standard], in which any bytes which would fail to decode are
	// decoded as themselves; it therefore never returns an error.
	//
	// [URL Living Standard]: https://url.spec.whatwg.org/#percent-encoded-bytes
	LaxDecoder transform.Transformer = laxDecoder{}
)
