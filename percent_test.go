package percent_test

import (
	"bytes"
	"crypto/rand"
	"io"
	"net/url"
	"os"
	"strings"
	"testing"

	"golang.org/x/text/transform"

	"codeberg.org/piman/percent"
)

var cases = map[string]struct {
	transform.Transformer
	cases map[string]string
}{
	"URIEncoder": {
		percent.URIEncoder,
		map[string]string{
			"":               "",
			" ":              "%20",
			"%":              "%25",
			"a/b":            "a%2Fb",
			"hello\x00world": "hello%00world",
		},
	},
	"Decoder": {
		percent.Decoder,
		map[string]string{
			" ":             " ",
			"":              "",
			"%20":           " ",
			"%25":           "%",
			"a%2Fb":         "a/b",
			"a%2fb":         "a/b",
			"hello%00world": "hello\x00world",
		},
	},
	"LaxDecoder": {
		percent.LaxDecoder,
		map[string]string{
			// valid cases for Decoder
			" ":             " ",
			"":              "",
			"%20":           " ",
			"%25":           "%",
			"a%2Fb":         "a/b",
			"a%2fb":         "a/b",
			"hello%00world": "hello\x00world",

			// failure cases for Decoder
			"%":      "%",
			"x%":     "x%",
			"%xx":    "%xx",
			"%ax":    "%ax",
			"%xa":    "%xa",
			"a%a%a":  "a%a%a",
			"%a":     "%a",
			"%\x00a": "%\x00a",
			"%a\x00": "%a\x00",

			// https://url.spec.whatwg.org/#example-percent-encode-operations
			"%25%s%1G": "%%s%1G",
			"‽%25%2E":  "\xE2\x80\xBD\x25\x2E",

			// others
			"abc%20%%20%": "abc % %",
		},
	},
}

func TestTransformers(t *testing.T) {
	for name, c := range cases {
		for input, want := range c.cases {
			pre := name + "/" + input + "/"

			// basic string to string transform
			t.Run(pre+"String", func(t *testing.T) {
				if got, _, err := transform.String(c.Transformer, input); err != nil {
					t.Error(err)
				} else if got != want {
					t.Errorf("String(%q) = %q; want %q", input, got, want)
				}
			})

			// appending to a too-small buffer
			t.Run(pre+"Append", func(t *testing.T) {
				var buf [1]byte
				if got, _, err := transform.Append(c.Transformer, buf[:0], []byte(input)); err != nil {
					t.Error(err)
				} else if string(got) != want {
					t.Errorf("Append([:0:1], %q) = %q; want %q", input, got, want)
				}
			})

			// writing one byte at a time, so partial
			// escapes must be buffered
			t.Run(pre+"Writer", func(t *testing.T) {
				var b strings.Builder
				w := transform.NewWriter(&b, c.Transformer)
				for _, v := range []byte(input) {
					if _, err := w.Write([]byte{v}); err != nil {
						t.Error(err)
					}
				}
				if err := w.Close(); err != nil {
					t.Error(err)
				}
				if got := b.String(); got != want {
					t.Errorf("Write(%q) = %q; want %q", input, got, want)
				}
			})
		}
	}
}

func TestDecoderFailure(t *testing.T) {
	for input, want := range map[string]url.EscapeError{
		"%":      "%",
		"x%":     "%",
		"%xx":    "%xx",
		"%ax":    "%ax",
		"%xa":    "%xa",
		"a%a%a":  "%a%",
		"%a":     "%a",
		"%\x00a": "%\x00a",
		"%a\x00": "%a\x00",
	} {
		t.Run(input, func(t *testing.T) {
			got, _, err := transform.String(percent.Decoder, input)
			if err != want {
				t.Errorf("Decode(%q) = %q; want %+v", input, got, want)
			}
		})
	}
}

func TestURIEncoderAll(t *testing.T) {
	decoded := make([]byte, 256)
	for i := range decoded {
		decoded[i] = byte(i)
	}
	encoded := []byte(
		"%00%01%02%03%04%05%06%07%08%09%0A%0B%0C%0D%0E%0F" +
			"%10%11%12%13%14%15%16%17%18%19%1A%1B%1C%1D%1E%1F" +
			"%20%21%22%23%24%25%26%27%28%29%2A%2B%2C-.%2F" +
			"0123456789%3A%3B%3C%3D%3E%3F" +
			"%40ABCDEFGHIJKLMNO" +
			"PQRSTUVWXYZ%5B%5C%5D%5E_" +
			"%60abcdefghijklmno" +
			"pqrstuvwxyz%7B%7C%7D%7E%7F" +
			"%80%81%82%83%84%85%86%87%88%89%8A%8B%8C%8D%8E%8F" +
			"%90%91%92%93%94%95%96%97%98%99%9A%9B%9C%9D%9E%9F" +
			"%A0%A1%A2%A3%A4%A5%A6%A7%A8%A9%AA%AB%AC%AD%AE%AF" +
			"%B0%B1%B2%B3%B4%B5%B6%B7%B8%B9%BA%BB%BC%BD%BE%BF" +
			"%C0%C1%C2%C3%C4%C5%C6%C7%C8%C9%CA%CB%CC%CD%CE%CF" +
			"%D0%D1%D2%D3%D4%D5%D6%D7%D8%D9%DA%DB%DC%DD%DE%DF" +
			"%E0%E1%E2%E3%E4%E5%E6%E7%E8%E9%EA%EB%EC%ED%EE%EF" +
			"%F0%F1%F2%F3%F4%F5%F6%F7%F8%F9%FA%FB%FC%FD%FE%FF")

	got, n, err := transform.Bytes(percent.URIEncoder, decoded)
	if err != nil || n != len(decoded) || string(got) != string(encoded) {
		t.Errorf("encode(%v) = %v, %d, %v; want %v, %d, %v",
			decoded, got, n, err,
			encoded, len(decoded), nil)
	}

	got, n, err = transform.Bytes(percent.Decoder, encoded)
	if err != nil || n != len(encoded) || string(got) != string(decoded) {
		t.Errorf("decode(%v) = %v, %d, %v; want %v, %d, %v",
			encoded, got, n, err,
			decoded, len(encoded), nil)
	}
}

func TestCaseInsensitive(t *testing.T) {
	all := make([]byte, 256)
	for i := range all {
		all[i] = byte(i)
	}
	enc, n, err := transform.Bytes(
		percent.NewEncoder(percent.Bytes{}),
		all,
	)
	if err != nil || n != len(all) {
		t.Errorf("encode(%v) = _, %d, %v; want _, %d, %v",
			all, n, err,
			len(all), nil)
	}

	got, n, err := transform.Bytes(percent.Decoder, bytes.ToLower(enc))
	if err != nil || n != len(enc) || string(got) != string(all) {
		t.Errorf("decode(%v) = %v, %d, %v; want %v, %d, %v",
			bytes.ToLower(enc), got, n, err,
			all, len(enc), nil)
	}

	got, n, err = transform.Bytes(percent.Decoder, bytes.ToUpper(enc))
	if err != nil || n != len(enc) || string(got) != string(all) {
		t.Errorf("decode(%v) = %v, %d, %v; want %v, %d, %v",
			bytes.ToUpper(enc), got, n, err,
			all, len(enc), nil)
	}
}

func benchmark(b *testing.B, decoded []byte) {
	b.Helper()

	mb := make([]byte, 1024*1024)
	for i := 0; i < len(mb); i += len(decoded) {
		copy(mb[i:], decoded)
	}

	b.Run("Encode", func(b *testing.B) {
		b.SetBytes(int64(len(mb)))
		w := transform.NewWriter(io.Discard, percent.URIEncoder)
		defer w.Close()
		for i := 0; i < b.N; i++ {
			if _, err := w.Write(mb); err != nil {
				b.Fatal(err)
			}
		}
	})

	encoded, _, err := transform.Bytes(percent.URIEncoder, mb)
	if err != nil {
		b.Fatal(err)
	}
	b.Run("Decode", func(b *testing.B) {
		b.SetBytes(int64(len(mb)))
		w := transform.NewWriter(io.Discard, percent.Decoder)
		defer w.Close()
		for i := 0; i < b.N; i++ {
			if _, err := w.Write(encoded); err != nil {
				b.Fatal(err)
			}
		}
	})
}

func BenchmarkEnglish(b *testing.B) {
	decoded, err := os.ReadFile("testdata/english.txt")
	if err != nil {
		b.Fatal(err)
	}
	benchmark(b, decoded)
}

func BenchmarkRussian(b *testing.B) {
	decoded, err := os.ReadFile("testdata/russian.txt")
	if err != nil {
		b.Fatal(err)
	}
	benchmark(b, decoded)
}

func BenchmarkRandom(b *testing.B) {
	var decoded [1024]byte
	if _, err := rand.Read(decoded[:]); err != nil {
		b.Fatal(err)
	}
	benchmark(b, decoded[:])
}

func FuzzTransform(f *testing.F) {
	f.Fuzz(func(t *testing.T, allowed []byte, s string) {
		t.Run("Roundtrip", func(t *testing.T) {
			var b strings.Builder
			w := transform.NewWriter(
				transform.NewWriter(&b, percent.Decoder),
				percent.NewEncoder(
					percent.Bytes{}.Allow(allowed...)),
			)
			if _, err := w.Write([]byte(s)); err != nil {
				t.Fatal(err)
			} else if err := w.Close(); err != nil {
				t.Fatal(err)
			} else if want, got := s, b.String(); got != want {
				t.Errorf("Decode(Encode(%q)) = %q", want, got)
			}
		})

		t.Run("PathUnescape", func(t *testing.T) {
			want, wanterr := url.PathUnescape(s)
			got, _, goterr := transform.String(percent.Decoder, s)
			if wanterr != goterr {
				t.Errorf("Decode(%q) = (%q, %v); want %v",
					s, got, goterr, wanterr)
			} else if wanterr == nil && want != got {
				t.Errorf("Decode(%q) = %q; want %v", s, got, want)
			}
		})
	})
}
